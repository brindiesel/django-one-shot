from django.urls import path, include
from todos.views import todo_list_list
from django.contrib import admin


urlpatterns = [
    path("", todo_list_list, name="todo_list_list"),
]
